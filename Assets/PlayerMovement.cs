using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D _rigidbody;
    private Vector2 movementInput;
    private Vector2 SmootheMovementInput;
    private Vector2 MovementSmootheMovementInput;
    [SerializeField]
    private float speed;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        SmootheMovementInput = Vector2.SmoothDamp(SmootheMovementInput, movementInput, ref MovementSmootheMovementInput, 0.1f);
        _rigidbody.velocity = SmootheMovementInput * speed;
    }

    private void OnMove(InputValue inputvalue)
    {
        movementInput = inputvalue.Get<Vector2>();
    }
}
